﻿//====================================================
//Created By RCBelmont On 2019年3月6日
//Description: 噪声生成模块
//====================================================

using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using Color = UnityEngine.Color;

namespace RCBelmont.NoiseGenerator
{
    public class NoiseGeneratorLib
    {
        private static int[] perm = new int[]
        {
            151, 160, 137, 91, 90, 15,
            131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
            190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
            88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
            77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
            102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
            135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
            5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
            223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
            129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
            251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
            49, 192, 21, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
            138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
        };

        public static Texture2D PerlinNoise1D(int resolution)
        {
            Texture2D tex = new Texture2D(resolution, resolution, TextureFormat.RGBA32, false);
            Color[] colorL = tex.GetPixels();
            float step = 1.0f / resolution;
            for (int i = 0; i < resolution; i++)
            {
                for (int j = 0; j < resolution; j++)
                {
                    Vector2 p0 = new Vector2(i * step, j * step);
                    colorL[i * resolution + j] = GetValue1D(p0, 10);
                }
            }

            tex.SetPixels(colorL);
            tex.Apply();
            return tex;
        }

        public static Texture2D PerlinNoise2D(int resolution, int freq = 2)
        {
            Texture2D tex = new Texture2D(resolution, resolution, TextureFormat.RGBA32, false);
            Color[] colorL = tex.GetPixels();
            float step = 1.0f / resolution;
            for (int i = 0; i < resolution; i++)
            {
                for (int j = 0; j < resolution; j++)
                {
                    Vector2 p0 = new Vector2(i * step, j * step);
                    colorL[i * resolution + j] = Color.white * (GetValue2D(p0, freq) * 0.5f + 0.5f);
                }
            }

            tex.SetPixels(colorL);
            tex.Apply();
            return tex;
        }

        public static Texture2D FractalNoise2D(int resolution, int freq = 2, int FractalCount = 5)
        {
            Texture2D tex = new Texture2D(resolution, resolution, TextureFormat.RGBA32, false);
            Color[] colorL = tex.GetPixels();
            float step = 1.0f / resolution;
            for (int i = 0; i < resolution; i++)
            {
                for (int j = 0; j < resolution; j++)
                {
                    float g = 0;
                    for (int k = 0; k < FractalCount; k++)
                    {
                        Vector2 p0 = new Vector2(i * step, j * step);
                        float a = Mathf.Pow(freq, k);
                        g += Mathf.Cos((GetValue2D(p0, a)) / a * 11 * 3.14f + (p0.x - 0.5f) * 3.14f *  2); 
                        //g += GetValue2D(p0, a) / a; 
                    }

                    g = g * 0.5f + 0.5f;
                    colorL[i * resolution + j] = Color.white * g;
                }
            }

            tex.SetPixels(colorL);
            tex.Apply();
            return tex;
        }

        #region PrivateMethod

        //OutputValue [-1, 1]
        private static float GetValue2D(Vector2 point, float freq)
        {
            Vector2[] grand = new[]
            {
                new Vector2(1, 0),
                new Vector2(-1, 0),
                new Vector2(0, 1),
                new Vector2(0, -1),
                new Vector2(1, -1).normalized,
                new Vector2(1, 1).normalized,
                new Vector2(-1, -1).normalized,
                new Vector2(-1, 1).normalized
            };
            point *= freq;
            int x0 = (int) point.x;
            int y0 = (int) point.y;
            int x1 = x0 + 1;
            int y1 = y0 + 1;
            int ix0 = x0 & 255;
            int ix1 = x1 & 255;
            int iy0 = y0 & 255;
            int iy1 = y1 & 255;
            float tx = point.x - x0;
            float ty = point.y - y0;


            Vector2 g00 = grand[perm[(perm[ix0] + iy0) & 255] & 7];
            Vector2 g01 = grand[perm[(perm[ix0] + iy1) & 255] & 7];
            Vector2 g10 = grand[perm[(perm[ix1] + iy0) & 255] & 7];
            Vector2 g11 = grand[perm[(perm[ix1] + iy1) & 255] & 7];
            Vector2 d00 = point - new Vector2(x0, y0);
            Vector2 d01 = point - new Vector2(x0, y1);
            Vector2 d10 = point - new Vector2(x1, y0);
            Vector2 d11 = point - new Vector2(x1, y1);
            float v00 = Vector2.Dot(d00, g00);
            float v01 = Vector2.Dot(d01, g01);
            float v10 = Vector2.Dot(d10, g10);
            float v11 = Vector2.Dot(d11, g11);
            tx = Smooth(tx);
            ty = Smooth(ty);
            float h = Mathf.Lerp(
                Mathf.Lerp(v00, v10, tx),
                Mathf.Lerp(v01, v11, tx)
                , ty);
            //h = Mathf.Lerp(v00, v10, tx);
            return h * Mathf.Sqrt(2);
        }

        private static Color GetValue1D(Vector2 point, float freq)
        {
            int[] grand = new[]
            {
                -1, 1
            };
            point *= freq;
            int x0 = (int) point.x;
            int x1 = x0 + 1;
            float t0 = point.x - x0;
            float t1 = point.x - x1;
            int g0 = grand[perm[x0 & 255] & 1];
            int g1 = grand[perm[x1 & 255] & 1];
            float t = Smooth(t0);
            float h = Mathf.Lerp(g0 * t0, g1 * t1, t);
            h = h + 0.5f;
            return Color.white * h;
        }

        private static float Smooth(float t)
        {
            //For 6*t^5 - 15*t^4 + 10*t^3
            return t * t * t * (t * (6 * t - 15) + 10);
        }

        #endregion
    }
}